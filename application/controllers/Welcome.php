<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use \Firebase\JWT\JWT;

class Welcome extends CI_Controller {
	public function index()
	{
		date_default_timezone_set('Asia/Jakarta');

		$key = "keyofjwttmp";
		$tmp = strtotime("2020-08-19 08:32:01");
		$payload = array(
			"nama" => "agung prabowo",
			"email" => "agung@mail.com",
			"notelp" => "085255223007",
			"tmp" => $tmp
		);
		$jwt = JWT::encode($payload, $key);
		$decoded = JWT::decode($jwt, $key, array('HS256'));
		print_r($jwt);
		
		echo "</br>";

		print_r($decoded);

		echo "</br>";

		$tmp = $decoded->tmp;
		echo "Timestamp: " . $tmp;

		echo "</br>";

		$now = time();
		echo "Sekarang: " . $now;

		echo "</br>";

		$selisih = $now-$tmp;
		$strselisih = gmdate("H:i:s", $selisih);

		echo "Selisih Timestamp: " . $selisih . " detik";

		echo "</br>";

		echo "String selisih: " . $strselisih;

		echo "</br>";

		if($selisih > 300) {
			echo "Selisih lebih dari 5 menit. (" . $strselisih . ")";
		} else {
			echo "Selisih kurang dari 5 menit. (" . $strselisih . ")";
		}
	}
}
